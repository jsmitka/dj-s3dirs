=============================
dj-s3dirs
=============================

.. image:: https://badge.fury.io/py/djs3dirs.png
    :target: http://badge.fury.io/py/djs3dirs
    
.. image:: https://travis-ci.org/jsmitka/djs3dirs.png?branch=master
        :target: https://travis-ci.org/jsmitka/djs3dirs

.. image:: https://pypip.in/d/djs3dirs/badge.png
        :target: https://crate.io/packages/djs3dirs?version=latest


Stores files in the current directory on Amazon S3.

Documentation
-------------

The full documentation is at http://djs3dirs.rtfd.org.

Quickstart
----------

Install dj-s3dirs::

    pip install djs3dirs

Then use it in a project::

	import djs3dirs

Features
--------

* TODO