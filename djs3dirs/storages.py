# -*- coding: utf-8 -*-

from __future__ import absolute_import

from django.conf import settings

from storages.backends.s3boto import S3BotoStorage


def get_storage(name=None, **kwargs):
    if name is None:
        name = ''.join([name.capitalize() for name in kwargs['location'].split('/')])
        name = '%sS3BotoStorage' % name

    return type(name, (S3BotoStorage, ), kwargs)


StaticS3BotoStorage = get_storage(name='StaticS3BotoStorage', location=settings.STATIC_ROOT)
MediaS3BotoStorage = get_storage(name='MediaS3BotoStorage', location=settings.MEDIA_ROOT)
