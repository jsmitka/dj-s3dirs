#!/usr/bin/env python

import os
import sys

import djs3dirs

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

version = djs3dirs.__version__

if sys.argv[-1] == 'publish':
    os.system('python setup.py sdist upload')
    print("You probably want to also tag the version now:")
    print("  git tag -a %s -m 'version %s'" % (version, version))
    print("  git push --tags")
    sys.exit()

readme = open('README.rst').read()
history = open('HISTORY.rst').read().replace('.. :changelog:', '')

setup(
    name='dj-s3dirs',
    version=version,
    description='Stores files in the current directory on Amazon S3.',
    long_description=readme + '\n\n' + history,
    author='Jindřich Smitka',
    author_email='jsmitka@smita.info',
    url='https://bitbucket.org/jsmitka/djs3dirs',
    packages=[
        'djs3dirs',
    ],
    include_package_data=True,
    install_requires=[
        'boto',
        'django-storages',
    ],
    license="BSD",
    zip_safe=False,
    keywords='djs3dirs',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Natural Language :: English',
        "Programming Language :: Python :: 2",
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
    ],
)
