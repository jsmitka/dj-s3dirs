============
Installation
============

At the command line::

    $ easy_install djs3dirs

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv djs3dirs
    $ pip install djs3dirs